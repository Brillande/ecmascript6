function newFunction (name, age, country) {
    var name = name || 'ellande';
    var age = age || 27;
    var country = country || 'ESP';
    console.log(name, age, country);
}

var hello = 'hello';
var world = 'world';
var epicPhrase = hello + " " + world;

console.log(epicPhrase);

//es6

function newFunction2 (name = 'oscar', age = 27, country = 'ESP') {
    console.log(name, age, country)
};

newFunction2();
newFunction2('ellande',27,'ESP');

let hello = 'hello';
let world = 'world';
let epicPhrase = `${hello} ${world}`:

console.log(epicPhrase):

